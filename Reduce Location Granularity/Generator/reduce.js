
  'use strict';
  

goog.provide('Blockly.Blocks.reduce');

goog.require('Blockly.Arduino');
  
  
  Blockly.Arduino['reduce'] = function(block) {
    var dropdown_location_granularity = block.getFieldValue('Location_Granularity');
    //var Value_LATLNG = Blockly.Arduino.valueToCode(block, 'LAT_LNG', Blockly.Arduino.ORDER_ATOMIC);
  
    Blockly.Arduino.definitions_["define_reduce"] = /*'const KEY = "AIzaSyBsC0Caxn8Hizhkl2H9KZtsIA1CaV63IF4";\n' + */'String url= "https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}"; \n' + 'String Reduced_Location; \n'
    //var LAT = "51.4492266";
    //var LNG = "-3.1793029";
    
   
  
    //var LATLNG = value_location.split(",",2);
   // var LAT= LATLNG[0].substring(1);
    //var LNG= LATLNG[1].substring(0,LATLNG[1].length-1);
    //let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}`;

   
     if (dropdown_location_granularity == "Full_Address")
     {
     var code = "";
      /*var code = 'url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}`; \n' +
    ' { \n' +
      ' fetch(url) \n' +
     ' .then(response => response.json()) \n' +
     '  .then(data => { \n' +
       '  console.log(data); \n' +
      ' let parts = data.results[0].address_components; \n' +
      ' window.alert(" Privacy issue! The Received Location is the Full Address which is too Granular :"+data.results[0].formatted_address); \n' +
     '}) \n' +
      ' .catch(err => console.warn(err.message)); \n' +

     ' }  \n'*/
      code = 'Serial.println("Full_Address");\n'
      }
  
   
    else if (dropdown_location_granularity == "Post_Code") 
    {
      var code = "";
      /*var code = 
      'url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}`; \n' +
    ' fetch(url) \n' +
      ' .then(response => response.json()) \n' +
      ' .then(data => { \n' +
        ' console.log(data); \n' +
        'let parts = data.results[0].address_components;\n' +
        
        'parts.forEach(part => {\n' +
    
          'if (part.types.includes("postal_code")) {\n' +
            'window.alert("The Granularity of the Location is reduced to Post Code Level: "+part.long_name);\n' +
          
       ' }\n' +
     ' }); \n' +
    '})\n' +
    '.catch(err => console.warn(err.message));\n' */
    code = ' Reduced_Location="CF11 0JW";\n' +
    'LocationArray[count] = Reduced_Location;\n'
  
    }

  else if (dropdown_location_granularity == "City_Name")
  {
    var code = "";
    /*var code = 'url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}`; \n' +
      ' fetch(url) \n' +
      ' .then(response => response.json()) \n' +
      '.then(data => { \n' +
        'console.log(data);\n' +
        'let parts = data.results[0].address_components;\n' +
        ' parts.forEach(part => { \n' 
          ' if (part.types.includes("administrative_area_level_2")) { \n' +
            ' window.alert("The Granularity of Location is reduced to City Level: "+part.long_name); \n' +
          '}\n' +
        '});\n' +
      '})\n' +
      '.catch(err => console.warn(err.message));\n';*/
       //code = 'Serial.print("City_Name");Serial.println("Cardiff");\n'
       code = ' Reduced_Location="Cardiff";\n' +
    'LocationArray[count] = Reduced_Location;\n'
  }

  else if (dropdown_location_granularity == "Country_Name")
  {
    var code = "";
    /*var code = 'url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}`; \n' +
      ' fetch(url) \n' +
      ' .then(response => response.json()) \n' +
      '.then(data => { \n' +
        'console.log(data);\n' +
        'let parts = data.results[0].address_components;\n' +
        ' parts.forEach(part => { \n' 
          ' if (part.types.includes("administrative_area_level_2")) { \n' +
            ' window.alert("The Granularity of Location is reduced to City Level: "+part.long_name); \n' +
          '}\n' +
        '});\n' +
      '})\n' +
      '.catch(err => console.warn(err.message));\n';*/
       //code = 'Serial.print("City_Name");Serial.println("Cardiff");\n'
       code = ' Reduced_Location="United Kingdom";\n' +
    'LocationArray[count] = Reduced_Location;\n'
  }

  //return Value_LATLNG;
  //return Value_LATLNG;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
  };