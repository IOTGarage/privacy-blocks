'use strict';

goog.provide('Blockly.Arduino.Minimise_Storage');

goog.require('Blockly.Arduino');

Blockly.Arduino.Minimise_Storage = function(block) {
    var dropdown_minimise_storage = block.getFieldValue('Store_Data');
    var retention_period = block.getFieldValue('Retention_Period');
    var code = '';

    switch (dropdown_minimise_storage) {
        case "Date":
            code = `
                // Minimize storage for Date
                EEPROM.update(0, Date);
                // Add retention logic
                if (millis() > ${retention_period} * 24 * 60 * 60 * 1000) {
                    EEPROM.update(0, 0); // Clear Date after retention period
                }
            `;
            break;

        case "Current_Time":
            code = `
                // Minimize storage for Current Time
                EEPROM.update(1, Current_Time);
                // Add retention logic
                if (millis() > ${retention_period} * 24 * 60 * 60 * 1000) {
                    EEPROM.update(1, 0); // Clear Current Time after retention period
                }
            `;
            break;

        case "Activity_Spent_Time":
            code = `
                // Minimize storage for Activity Spent Time
                EEPROM.update(2, Activity_Spent_Time);
                // Add retention logic
                if (millis() > ${retention_period} * 24 * 60 * 60 * 1000) {
                    EEPROM.update(2, 0); // Clear Activity Spent Time after retention period
                }
            `;
            break;

        case "Location_Data":
            code = `
                // Minimize storage for Location Data
                EEPROM.update(3, Location_Data);
                // Add retention logic
                if (millis() > ${retention_period} * 24 * 60 * 60 * 1000) {
                    EEPROM.update(3, 0); // Clear Location Data after retention period
                }
            `;
            break;

        case "Heart_Rate":
            code = `
                // Minimize storage for Heart Rate
                EEPROM.update(4, Heart_Rate);
                // Add retention logic
                if (millis() > ${retention_period} * 24 * 60 * 60 * 1000) {
                    EEPROM.update(4, 0); // Clear Heart Rate after retention period
                }
            `;
            break;

        case "Total_Calories":
            code = `
                // Minimize storage for Total Calories
                EEPROM.update(5, Calories);
                // Add retention logic
                if (millis() > ${retention_period} * 24 * 60 * 60 * 1000) {
                    EEPROM.update(5, 0); // Clear Calories after retention period
                }
            `;
            break;

        case "Speed":
            code = `
                // Minimize storage for Speed
                EEPROM.update(6, Speed_Data);
                // Add retention logic
                if (millis() > ${retention_period} * 24 * 60 * 60 * 1000) {
                    EEPROM.update(6, 0); // Clear Speed after retention period
                }
            `;
            break;

        case "Distance":
            code = `
                // Minimize storage for Distance
                EEPROM.update(7, Distance);
                // Add retention logic
                if (millis() > ${retention_period} * 24 * 60 * 60 * 1000) {
                    EEPROM.update(7, 0); // Clear Distance after retention period
                }
            `;
            break;

        default:
            code = '// Unknown storage type';
            break;
    }

    return code;
};
