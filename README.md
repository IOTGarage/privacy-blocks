# Privacy Blocks



## Description
Privacy Blocks are privacy components designed according to Privacy by Design (PbD) guidelines and the Combined Privacy Law Framework (CPLF). They assist software developers in integrating privacy-preserving measures into their applications while enhancing awareness of privacy practices and compliance with data protection laws.

## Design and Implementation of Privacy-Preserving Components

- **Reduce Location Granularity**: This block reduces the user's location granularity by performing reverse geocoding to convert geographic coordinates (latitude and longitude) received by the GPS module into a human-readable address (full address).
- **Minimize Raw Data Intake**: This block reduces the amount of raw data it receives by calculating an average of the sensor data values over a specified period of time.
- **Category-based Aggregation**: This block reduces the granularity of the raw data into predefined categories.
- **Minimize Data Storage and Retention Period**: This block minimizes the amount of data stored by an IoT application and the duration for which data is stored. It helps avoid retaining data for longer than needed.
- **Access Control**: Access Control ensures that information is processed legitimately and by an authorized party to prevent an activity that could lead to a security breach.
- **Data Anonymization**: This block suggests different data anonymization techniques for anonymizing personally identifiable information before the data gets used by IoT applications.
- **Encrypted Data Storage**:  This block suggests encryption techniques based on type.
- **Encrypted Data Communication**: This block encrypts and sends data using a specified protocol. Encrypted data communication reduces potential privacy risks due to unauthorized access during data transfer.




## Usage

###  Blockly@rduino:

- To install it locally. Download Canella-Blockly@rduino in <a href="https://gitlab.com/IOTGarage/canella" title="Canella Repository"> and open ../Blockly@rduino/index.html

### Examples

Example configurations and usage scenarios can be found in the documentation and the provided demo video [here](https://www.youtube.com/watch?v=7V0q3MyAzCQ&t=75s).


## Prototyping: Fitness Tracking IoT Application

A hypothetical use case scenario demonstrates how Canella helps integrate privacy-preserving components into the data flow of a Fitness Tracking IoT application.  It is a helpful IoT-based system that enables trainees to track their activities easily. Additionally, it assists trainers in managing their trainees, easily monitoring their progress, and encouraging them for their health, wellness, and safety.

![Fitness Tracking IoT Application](./images/UseCase.jpg)

## Canella
Canella is an integrated IoT development ecosystem designed to prioritize privacy in IoT applications. It leverages End-User Development (EUD) tools such as Blockly@rduino and Node-RED to help developers create end-to-end IoT applications that comply with privacy regulations. Canella provides real-time feedback on privacy concerns, streamlines privacy implementation, and reduces cognitive load, thus enhancing productivity and privacy awareness among developers.

### Link to Canella for Prototyping
- For detailed insights into the Canella tool, including its source code and documentation, visit the link to the <a href="https://gitlab.com/IOTGarage/canella" title="Canella Repository"> Canella Repository. </a>


### Link to Privacy Nodes
- Detailed insights into the Privacy Nodes are available at the <a href="https://gitlab.com/IOTGarage/privacy-nodes" title="Privacy Nodes Repository"> Privacy Nodes Repository. </a>