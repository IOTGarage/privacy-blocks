'use strict';

goog.provide('Blockly.Arduino.category_aggregation'); 
goog.require('Blockly.Arduino');

Blockly.Arduino.category_aggregation = function(block) {


var value_Heart_Rate = block.getFieldValue('Heart_Rate_Value');
var value_Active_Calories = block.getFieldValue('Active_Calories_Value');
var value_Speed = block.getFieldValue('Speed_Value');
var value_Distance = block.getFieldValue('Distance_Value');
var value_Spent_Time = block.getFieldValue('Spent_Time_Value');


Blockly.Arduino.includes_["includes_Category_Based_Aggregation_Def"] = 'int average = 0;\n'


//Heart Rate Data 
if (value_Spent_Time)
{
  // TODO: Assemble JavaScript into code variable.

var code = "";
var code = 
'if (millis() > WorkoutInterval) //To apply category-based aggregation guideline on activity spent time  for particular workout\n'+
'{\n'+
'String Spent_Time_Range1 = String(Second+2);\n'+
'String Spent_Time_Range2 = String(Second-2);\n'+
'SpentTimeValue = "Time Range "+ Spent_Time_Range1+" and " +Spent_Time_Range2;\n'+
'}\n'

}

else if (value_Heart_Rate == "Range")  // if Range is selected print the reange of Heart Rate Beat per Minute
{
// TODO: Assemble JavaScript into code variable.

var code = "";
var code = 
' if (millis() > WorkoutInterval) //To aggregate heart rate during a particular work out \n'+
'{ \n'+
'average = total / count;\n'+
'if (average >= 60 && average <= 100)\n'+
'{\n'+
'Heart_Rate = "The average range of heart rate beat is between 60 and 100 BPM";\n'+
'}\n'+
'else if (average < 60 )\n'+
'{\n'+
'Heart_Rate = "The average range of heart rate beat is less than 60 BPM";\n'+
'}\n'+
'else if (average > 100 )\n'+
'{\n'+
'Heart_Rate = "The average range of heart rate beat is greater than 100 BPM";\n'+
'}\n'+
'}\n'
}
else if  (value_Heart_Rate == "Status") // if Status is selected print the statues of Heart Rate (Low,Noraml, or High)

{
    var code = "";
    var code = 
    ' if (millis() > WorkoutInterval) //To aggregate heart rate during a particular work out \n'+
    ' { \n'+
    ' average = total / count; \n'+
    ' if (average >= 60 && average <= 100) \n'+
    '{\n'+
    'Heart_Rate= "Average Heart Rate Status is Normal"; \n'+
    '}\n'+
    'else if (average < 60 )\n'+
    '{\n'+
    'Heart_Rate = "Average Heart Rate Status is Low";\n'+
    '}\n'+
    'else if (average > 100 )\n'+
    '{\n'+
    'Heart_Rate = "Average Heart Rate Status is High";\n'+
    '}\n'+
    '}\n'
}



if (value_Active_Calories)
{
  var code = "";
  var code =  
 'if (millis() > WorkoutInterval) //To apply category-based aggregation guideline on total calories data for particular workout\n'+
  '{\n'+
  'String Total_Calorories_Range1 = String(Total_calories-20);\n'+
  'String Total_Calorories_Range2 = String(Total_calories+20);\n'+
  'Calories = "CAL Range"+ Total_Calorories_Range1+" and " +Total_Calorories_Range2;\n'+
 '}\n'
}

if (value_Speed)
{
  var code = "";
  var code =  
  'if (millis() >  WorkoutInterval) //To print the average heart rate for a specified period during a particular workout \n' +
  '{\n' +
  'int AverageSpeed= Total_Speed/count;\n' +

  'String Speed_Range1 = String(AverageSpeed-20);\n' +
  'String Speed_Range2  = String(AverageSpeed+20);\n' +
  'Speed_Data = "Speed range"+ Speed_Range1+" and " +Speed_Range2;\n'+
'}\n' 
  
}

if (value_Distance )
{

  var code = "";
  var code =  
  'if (millis() > WorkoutInterval) //To apply category-based aggregation guideline on distance data for particular workout\n'+
  '{\n'+
  'String Distnce_Range1 = String(Distance-20);\n'+
  'String Distnce_Range2  = String(Distance+20);\n'+
  'String(Distance) = "Distance range is between "+ Distnce_Range1+" and " +Distnce_Range2;\n'+
 '}\n'
}
return [code, Blockly.Arduino.ORDER_ATOMIC];
};




